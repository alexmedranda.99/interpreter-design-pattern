﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploInterpreter
{
    class TenExpression:Expression
    {
        public override string One() { return "X"; }//Devuelve X
        public override string Four() { return "XL"; }//Devuelve XL
        public override string Five() { return "L"; }//Devuelve L
        public override string Nine() { return "XC"; }//Devuelve XC
        public override int Multiplier() { return 10; }//Devuelve 10
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploInterpreter
{
    abstract class Expression

    {
        public void Interpret(Context context)//Definimos el interprete teniendo como datos el contexto
        {
            if (context.Input.Length == 0)
                return;
            // Substring:  En este método extraemos un fragmento de una cadena existente.
            // StartsWith: Determina si el comienzo de esta instancia de cadena coincide con una cadena especificada.
            if (context.Input.StartsWith(Nine()))//9-90-900
            {
                context.Output += (9 * Multiplier());
                context.Input = context.Input.Substring(2);//
            }
            else if (context.Input.StartsWith(Four()))//4-40-400
            {
                context.Output += (4 * Multiplier());
                context.Input = context.Input.Substring(2);//
               
            }
            else if (context.Input.StartsWith(Five()))//5-50-500
            {
                context.Output += (5 * Multiplier());
                context.Input = context.Input.Substring(1);//L
            
            }

            while (context.Input.StartsWith(One()))//1-10-100-1000
            {
                context.Output += (1 * Multiplier());
                context.Input = context.Input.Substring(1);
                
            }
        }

        public abstract string One();//Metodo abstracto one
        public abstract string Four();//Metodo abstracto four
        public abstract string Five();//Metodo abstracto five
        public abstract string Nine();//Metodo abstracto nine
        public abstract int Multiplier();//Metodo abstracto Multi[pler

    }
}

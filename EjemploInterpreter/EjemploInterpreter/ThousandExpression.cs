﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploInterpreter
{
    class ThousandExpression:Expression
    {
        public override string One() { return "M"; }//Devuelve M
        public override string Four() { return " "; }//Devuelve cadena vacía
        public override string Five() { return " "; }//Devuelve cadena vacía
        public override string Nine() { return " "; }//Devuelve cadena vacía
        public override int Multiplier() { return 1000; }//Devuelve 1000
    }
}

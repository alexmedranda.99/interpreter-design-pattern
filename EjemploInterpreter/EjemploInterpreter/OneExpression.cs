﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploInterpreter
{
    class OneExpression: Expression
    {
        public override string One() { return "I"; }//Devuelve I
        public override string Four() { return "IV"; }//Devuelve IV
        public override string Five() { return "V"; }//Devuelve V
        public override string Nine() { return "IX"; }//Devuelve IX
        public override int Multiplier() { return 1; }//Devuelve 1
    }
}

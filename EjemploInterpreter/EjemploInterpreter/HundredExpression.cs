﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploInterpreter
{
    class HundredExpression: Expression
    {
        public override string One() { return "C"; }//Devuelve C
        public override string Four() { return "CD"; }//Devuelve CD
        public override string Five() { return "D"; }//Devuelve D
        public override string Nine() { return "CM"; }//Devuelve CM
        public override int Multiplier() { return 100; }//Devuelve 100
    }
}

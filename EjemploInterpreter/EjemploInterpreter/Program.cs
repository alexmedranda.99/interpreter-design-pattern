﻿using System;
using System.Collections.Generic;

namespace EjemploInterpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            string roman = "MCMXXVIII";//1928 - Valor de entrada
            Context context = new Context(roman);//Creando objeto contexto

            // Construye el 'parse tree'

            List<Expression> tree = new List<Expression>();
            tree.Add(new ThousandExpression());
            tree.Add(new HundredExpression());
            tree.Add(new TenExpression());
            tree.Add(new OneExpression());

            // Interpret


            foreach (Expression exp in tree)
            {
                exp.Interpret(context);
            }

            Console.WriteLine("{0} = {1}",
              roman, context.Output);

            // Espera al Usuario

            Console.ReadKey();
        }
    }
}
